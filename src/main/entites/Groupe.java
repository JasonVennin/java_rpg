package main.entites;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Groupe {
	
	private List<Combattant> combattants;
	private String nom;

	
	public Groupe() {
		// TODO Auto-generated constructor stub
	}
	
	public Groupe(String nom, List<Combattant> combattants) {
		this.combattants = combattants;
		this.nom = nom;
	}
	
	/**
	 * retourne la liste des combattants encore en vie
	 * @return une liste d'instance combattants qui sont en vie
	 */
	public List<Combattant> getCombattantsAlive(){
		List<Combattant> combattantsAlive = new ArrayList<Combattant>();
		for (Combattant combattant : this.combattants) {
			if(combattant.isAlive()) {
				combattantsAlive.add(combattant);
			}
		}
		return combattantsAlive;
	}
	
	/**
	 * ajoute un combattant au groupe
	 * @param combattant une instance de la classe combattant
	 */
	public void addCombattant(Combattant combattant) {
		this.combattants.add(combattant);
	}
	
	/**
	 * Verifie si tous les membres du groupes sont morts
	 * @return true si tous les Combattants du groupe sont morts.
	 */
	public boolean estMort() {
		boolean res = true;
		for (Combattant combattant : combattants) {
			if (combattant.isAlive()) {
				res = false;
			}
		}
		return res;
	}


	/**
	 * Permet de choisir aleatoirement un combattant en vie et d'attaquer un  groupe adverse
	 * @param adversaire une instance de la classe groupe
	 */
	public void attaquer(Groupe adversaire) {
		Random rand = new Random();
		List<Combattant> combattantsAlive = this.getCombattantsAlive();
	    Combattant randomCombattantAlive = combattantsAlive.get(rand.nextInt(combattantsAlive.size()));
		adversaire.defendre(randomCombattantAlive);
		
	}	
	
	/**
	 * Permet de choisir un membre du groupe aleatoire qui va subir les degats du combattant adverse
	 * @param combattant une instance de la classe combattant qui va attaquer un membre de notre groupe aleatoirement
	 */
	public void defendre(Combattant combattant) {
		Random rand = new Random();
		List<Combattant> combattantsAlive = this.getCombattantsAlive();
	    Combattant randomCombattantAlive = combattantsAlive.get(rand.nextInt(combattantsAlive.size()));
	    combattant.attaquer(randomCombattantAlive);
	}

	public List<Combattant> getCombattants() {
		return combattants;
	}

	public void setCombattants(List<Combattant> combattants) {
		this.combattants = combattants;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
