package main.entites;

import main.Classe;

public class Personnage extends AbstractCombattant{
	
	private Classe classe;
	
	
	public Personnage() {
		super();
	}
	
	public Personnage(String nom, int pointDeVie, int degats, Classe classe) {
		super(nom, pointDeVie, degats);
		this.classe = classe;
	}
	
	
	/**
	 * Permet d'attaquer l'entite passée en parametre. (Ses points de vie se voit reduit de vos degats)
	 * @param combattants le combattantq que vous attaquez 
	 */
	public void attaquer(Combattant combattant) {
		combattant.defendre(this.classe.getAttaque().lancerAttaque());
	}
	
	
}
