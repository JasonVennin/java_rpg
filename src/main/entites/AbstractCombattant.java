package main.entites;

public abstract class AbstractCombattant implements Combattant{
	
	private String nom;
	private int pointDeVie;
	private int degats;
	
	public AbstractCombattant() {
		// TODO Auto-generated constructor stub
	}
	
	public AbstractCombattant(String nom, int pointDeVie, int degats) {
		this.nom = nom;
		this.pointDeVie = pointDeVie;
		this.degats = degats;
	}
	
	/**
	 * Regarde si l'entite est en vie
	 * @return un boolean, true si il est en vie , false sinon
	 */
	public boolean isAlive() {
		return this.getPointDeVie()>0;
	}
	
	/**
	 * Permet d'attaquer l'entite passée en parametre. (Ses points de vie se voit reduit de vos degats)
	 * @param combattants le combattantq que vous attaquez 
	 */
	public void attaquer(Combattant adversaire) {
		adversaire.defendre(this.degats);
	}
	
	/**
	 * Enleve aux points de vie les degats passé en params
	 */
	public void defendre(int degats) {
		this.pointDeVie -= degats;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPointDeVie() {
		return pointDeVie;
	}

	public void setPointDeVie(int pointDeVie) {
		this.pointDeVie = pointDeVie;
	}

	public int getDegats() {
		return degats;
	}

	public void setDegats(int degats) {
		this.degats = degats;
	}

	@Override
	public String toString() {
		return "nom:[" + this.getNom() + "], pointDeVie:[" + this.getPointDeVie() + "], attaque:[" + this.getDegats() + "]" ;
	}

}
