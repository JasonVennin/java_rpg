package main;

import java.util.ArrayList;
import java.util.List;

import main.config.Config;
import main.config.Factory;
import main.entites.Combattant;
import main.entites.Groupe;
import main.entites.Monstre;
import main.entites.Personnage;

public class Monde {

	/**
	 * Tant qu'un personnage est en vie alors les deux entites s'attaque a tour de role
	 * @param personnage une instance de personnage 
	 * @param monstre une instance de monstre
	 */
	public static void combat(Personnage personnage, Monstre monstre){
			while(personnage.isAlive() && monstre.isAlive()) {
				personnage.attaquer(monstre);
				if(monstre.isAlive()) {
					monstre.attaquer(personnage);
				}
			}
			quiGagne(personnage, monstre);
	}
	
	/**
	 * Affiche qui gagne
	 * @param personnage une instance de la classe personnage
	 * @param monstre une instance de la classe monstre
	 */
	public static void quiGagne(Personnage personnage, Monstre monstre) {
		if(personnage.isAlive()) {
			System.out.println(personnage.getNom() + " a gagné !");
		}
		else {
			System.out.println(monstre.getNom() + " a gagné !");
		}
	}
	
	/**
	 * Tant que tout les membres d'un groupe ne sont pas mort alors les deux entites s'attaque a tour de role
	 * @param groupe1 une instance de groupe contenant des combattants
	 * @param groupe2 une instance de groupe contenant des combattants
	 */
	public static void combat(Groupe groupe1, Groupe groupe2){
			while(!groupe1.estMort() && !groupe2.estMort()) {
				groupe1.attaquer(groupe2);
				if(!groupe2.estMort()) {
					groupe2.attaquer(groupe1);
				}
			}
			if(!groupe1.estMort()) {
				System.out.println(groupe1.getNom() + " a gagné !");
			}
			else {
				System.out.println(groupe2.getNom() + " a gagné !");
			}
		}

	/**
	 * lance un combat 1v1
	 */
	public static void combat1v1() {
		combat(Factory.personnageFactory(), Factory.monstreFactory());
	}
	
	/**
	 * Lance un combat groupe contre groupe
	 */
	public static void combatGroupe() {
		int taille1 = Config.demanderInt(" Quel taille voulez vous pour votre groupe de personnage ?");
		int taille2 = Config.demanderInt(" Quel taille voulez vous pour votre groupe de monstre ?");
		Monde.combat(Factory.groupePersonnageFactory(taille1), Factory.groupeMonstreFactory(taille2));
	}
	
	/**
	 * Lance un combat entre 1 persoonage et un groupe de monstre
	 */
	public static void combat1vWorld() {
		int taille = Config.demanderInt(" Quel taille voulez vous pour votre groupe de monstre ?");
		Combattant personnage = Factory.personnageFactory();
		List<Combattant> listeCombattant = new ArrayList<Combattant>();
		listeCombattant.add(personnage);
		Groupe groupeSolo = new Groupe(personnage.getNom(), listeCombattant);
		Monde.combat(groupeSolo, Factory.groupeMonstreFactory(taille));
	}
	
	public static void afficherInformations() {
		System.out.println("ceci est un monde");
	}
	
}
