package main;
import java.util.List;
import java.util.Random;

import main.attaques.Attaque;

public class Classe {
	
	private String nom;
	private List<Attaque> attaques;
	
	public Classe() {
		// TODO Auto-generated constructor stub
	}
	
	public Classe(String nom, List<Attaque> attaques) {
		this.nom = nom;
		this.attaques = attaques;
	}
	
	/**
	 * Renvoie une attaque aleatoire parmis la liste des attaques de l'attribut attaques
	 * @return une instance de la classe attaque
	 */
	public Attaque getAttaque() {
		Random rand = new Random();
	    Attaque randomAttaque = this.attaques.get(rand.nextInt(this.attaques.size()));
		return randomAttaque;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Attaque> getAttaques() {
		return attaques;
	}

	public void setAttaques(List<Attaque> attaques) {
		this.attaques = attaques;
	}

}
