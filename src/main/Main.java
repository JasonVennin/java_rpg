package main;

import java.util.Scanner;
import main.config.Factory;

public class Main {
	
	public static void lancerJeu() {
		Factory.initMapClasses();
		Scanner scanner = new Scanner(System.in);
		printChoix();
		int choix = scanner.nextInt();
		while(choix<0 || choix >4) {
			System.out.println("Choix invalide");
			printChoix();
			choix = scanner.nextInt();
		}
		if(choix == 1) {
			Monde.combat1v1();
		}
		else if(choix == 2) {
			Monde.combatGroupe();
		}
		else if(choix == 3) {
			Monde.combat1vWorld();
		}
		else if(choix == 4) {
			Monde.afficherInformations();
		}
		scanner.close();
	}
	
	
	
	
	
	/**
	 * affiche les choix du menu
	 */
	public static void printChoix() {
		System.out.println("---***--- Bonjour ---***---");
		System.out.println("   Choisir une option");
		System.out.println("   1: Lancer un combat 1v1");
		System.out.println("   2: Lancer un combat de Groupe");
		System.out.println("   3: One vs World Hardcore Edition");
		System.out.println("   4: Informations");
	}
	
	
	public static void main(String[] args) {
		lancerJeu();
	}	

}
