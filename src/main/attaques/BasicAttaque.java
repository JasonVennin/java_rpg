package main.attaques;

import java.util.Random;

public class BasicAttaque implements Attaque {
	
	private String nom;
	private int degats;
	private double chanceToucher;
	
	public BasicAttaque() {
		// TODO Auto-generated constructor stub
	}
	
	public BasicAttaque(String nom, int degats, double chanceToucher) {
		this.nom = nom;
		this.degats = degats;
		this.chanceToucher = chanceToucher;
	}
	
	/**
	 * Permet de verifier si le lanceur touche l'attaque ou non.
	 * @param lanceur une instance de type Combattant emetteur de l'attaque
	 * @param cible une instance de type combattant recepteur de l'attaque
	 * @return un integer de la valeur de l'attaque , zero si l'attaque est loupé 
	 */
	public int lancerAttaque() {
		// Génère un nombre aleatoire compris entre 0 et 100
		Random random = new Random();
		int nombreAlea = random.nextInt(99) + 1;
		
		if(nombreAlea < this.chanceToucher) {
			return this.degats;
		}
		else {
			System.out.println("Echec de l'attaque");
			return 0;
		}
	}


	@Override
	public String getNom() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNom(String nom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getDegats() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setDegats(int degats) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public double getChanceToucher() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void setChanceToucher(double chanceToucher) {
		// TODO Auto-generated method stub
		
	}

}
