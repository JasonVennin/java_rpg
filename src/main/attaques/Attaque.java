package main.attaques;


public interface Attaque {
	
	public int lancerAttaque();
	public String getNom();
	public void setNom(String nom);
	public int getDegats();
	public void setDegats(int degats);
	public double getChanceToucher();
	public void setChanceToucher(double chanceToucher);


}
