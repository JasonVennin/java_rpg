package main.config;

import java.util.Scanner;

public class Config {
	
	/**
	 * Permet de demander un string  en affichant un message dans la console 
	 * @return un string que l'utilisateur a entré
	 */
	public static String demanderString(String msg) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(msg);
		String res = scanner.nextLine();
		return res;
	}
	
	/**
	 * Permet de demander un string  en affichant un message dans la console 
	 * @return un string que l'utilisateur a entré
	 */
	public static int demanderInt(String msg) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(msg);
		int res = scanner.nextInt();
		return res;
	}

}
