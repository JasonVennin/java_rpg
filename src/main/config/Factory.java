package main.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import main.Classe;
import main.attaques.Attaque;
import main.attaques.BasicAttaque;
import main.entites.Combattant;
import main.entites.Groupe;
import main.entites.Monstre;
import main.entites.Personnage;

public class Factory {
	
	static String[] debutNom = {"chat","chien","chaton", "elephant"};
	static String[] finNom = {" mechant"," de feu"," de la mort", "de glace"};
	static Map<Integer, Classe> classes = new HashMap<Integer, Classe>();

	
	/**
	 * Permet d'initialiser des classes 
	 */
	public static void initMapClasses() {
		classes.put(1, Factory.genererClasseWithBasicAttaque("Neutre", 20, 80));
		classes.put(2, Factory.genererClasseWithBasicAttaque("Hasardeux", 30, 50));
		classes.put(3, Factory.genererClasseWithBasicAttaque("QuitteOuDouble", 40, 10));
	}
	
	/**
	* Créer un personnage avec tous ses attributs.
	* @return une instance de la classe Personnage correctement instancié.
	**/
	public static Personnage personnageFactory(){
		String nomPersonnage = Config.demanderString("Veuillez entrer le nom de votre personnage : ");
		int choixClasse = demanderClasse();
		int pointDeVie = Config.demanderInt("Veuillez entrer les points de vie du personnage");
		int degats = Config.demanderInt("Veuillez entrer les degats du personnage");
		Personnage personnage = new Personnage(nomPersonnage, pointDeVie, degats, classes.get(choixClasse));
		
		return personnage;
	}
	
	/**
	* Créer un monstre avec tous ses attributs.
	* Demande a l'utilisateur d'entrer le nom du Monstre ses ppoints de vie et ses degats.
	* @return une instance de la classe Monstre correctement instancié
	**/
	public static Monstre monstreFactory() {
		// genere un nom aleatoire pour un monstre
		String nomMonstre = genererNom();
		int pointDeVie = Config.demanderInt("Veuillez entrer les points de vie du monstre");
		int degats = Config.demanderInt("Veuillez entrer les degats du monstre");
		// Créer un nouveau monstre en utilisant le constructeur
		// avec tous ses params (dont le nom qui vient d'etre choisie par l'utilisateur)
		Monstre monstre = new Monstre(nomMonstre, pointDeVie, degats);
		// Retourner l'instance du monstre
		return monstre;
	}
	
	/**
	 * genere un nom aleatoire pour le monstre
	 * @return un string representant un nom de monstre
	 */
	public static String genererNom() {
		Random rand = new Random();
	    String randomDebutNom = debutNom[rand.nextInt(debutNom.length)];
	    String randomFinNom = finNom[rand.nextInt(finNom.length)];
		
		String res = randomDebutNom + randomFinNom;
		return res;
	}
	
	
	/**
	 * Genere un groupe de monstre de taille tailleGroupe
	 * @param tailleGroupe le nombre de monstre dans votre groupe
	 * @return une instance de la classe groupe composse  de monstre 
	 */
	public static Groupe groupeMonstreFactory(int tailleGroupe) {
		List<Combattant> listeMonstre = new ArrayList<Combattant>();
		for(int i = 0; i < tailleGroupe; i++) {
			listeMonstre.add(monstreFactory());
		}
		Groupe res = new Groupe("Groupe de monstre", listeMonstre);
		return res;
	}
	
	/**
	 * Genere un groupe de personnage de taille tailleGroupe
	 * @param tailleGroupe le nombre de personnage dans votre groupe
	 * @return une instance de la classe groupe composse  de personnage 
	 */
	public static Groupe groupePersonnageFactory(int tailleGroupe) {
		List<Combattant> listePersonnage = new ArrayList<Combattant>();
		for(int i = 0; i < tailleGroupe; i++) {
			listePersonnage.add(personnageFactory());
		}
		String nomGroupe = Config.demanderString("Veuillez entrer le nom de votre groupe de personnage : ");
		Groupe res = new Groupe(nomGroupe, listePersonnage);
		return res;
	}
	
	/**
	 * genere une classe avec le nom, degats et chance de toucher entrer en param
	 * @return une instance de la classe Classe
	 */
	public static Classe genererClasseWithBasicAttaque(String nom, int degats, double chanceToucher) {
		List<Attaque> attaques = new ArrayList<Attaque>();
		BasicAttaque basicAttaque = new BasicAttaque(nom, degats, chanceToucher);
		attaques.add(basicAttaque);
		Classe classe = new Classe(nom, attaques);
		return classe;
	}
	
	/**
	 * Permet de demander en affichant toutes les classes la classe que le joueur veut prendre
	 * @return un int representant la clef dans le dictionnaire
	 */
	public static int demanderClasse() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Choisissez une classe : ");
		for (int key : classes.keySet()) {
			System.out.println(key + " : " + classes.get(key).getNom());
		}
		int choix = scanner.nextInt();
		
		while(choix <0 || choix > classes.keySet().size()) {
			System.out.println("Choix invalide");
			for (int key : classes.keySet()) {
				System.out.println(key + " : " + classes.get(key).getNom());
			}
			choix = scanner.nextInt();
		}
		return choix;
	}
	
	
	
}
